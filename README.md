# MaX v2

## Prérequis
 - Java

## Dépendances
 - BaseX
 - Saxon

Celles-ci sont automatiquement installées par la commande `make install`

## Installation

``
make help
``

## Démarrage rapide

Installation de MaX, du corpus de démonstration et lancement du service :

```
make install && make demo && make run
```

Note :  `.max/basex/webapp/max/` pointe vers  `src/main`, ce symlink est automatiquement créé par la commande `make install`.

## Organisation des sources

- `src` : toutes les sources
  - `main` : sources applicatives
    - `core`
    - `bundles`
  - `resources` : ressources additionnelles  
  - `test` : source des tests (@todo)

Ce dépôt contient les bundles 
 - `max-dev` : aide au développement ([README](src/main/bundles/max-dev/README.md)).
 - `max-sources` : export des sources XML ([README](src/main/bundles/max-sources/README.md)).
 - `max-dumb-xml` : bundle de vocabulaire d'exemple.


## Le dossier .max

Il contient le serveur BaseX. C'est un dossier (caché) purement technique dans lequel l'utilisateur n'aura pas (et ne devra pas) intervenir.


## Documentations

Consultables [ici](https://git.unicaen.fr/pdn-certic/max-documentation/-/tree/max-v2/docs)

