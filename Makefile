SRC_DIR=${CURDIR}/src

install-basex: # BaseX & dépendances: Téléchargement, installation et définition du mot de passe admin
	@if [ -d '.max/basex' ]; then\
		echo 'Installation de BaseX : OK.';\
	else\
	  	mkdir .max;\
		cd .max;\
		curl https://files.basex.org/releases/11.1/BaseX111.zip --output BaseX111.zip;\
		unzip BaseX111.zip;\
		curl https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/10.8/Saxon-HE-10.8.jar --output Saxon-HE-10.8.jar;\
		mv Saxon-HE-10.8.jar basex/lib/custom/;\
		rm BaseX111.zip;\
		cd ..;\
		echo 'Création de la base "max".';\
		./.max/basex/bin/basex -c"CREATE DB max";\
		echo 'Création du fichier de configuration par défaut "config.xml"';\
		cp src/resources/config.xml.sample config.xml;\
		echo 'Installation de BaseX terminée.';\
    fi
.PHONY: install-basex


install: install-basex ## Installation de MaX
	@if [ -d '.max/basex/webapp/max' ]; then\
		echo 'Installation de MaX : OK.';\
	else\
		ln -s $(SRC_DIR)/main/ .max/basex/webapp/max;\
		ln -s $(SRC_DIR)/resources .max/resources;\
		ln -s ../fixtures .max/fixtures;\
		echo 'Installation de MaX effectuée.';\
	fi
.PHONY: install


tests: ## Exécution des tests unitaires
	$(shell cp src/resources/config.xml.sample config.xml)
	-@.max/basex/bin/basex -c"TEST $(SRC_DIR)/test" > tests-output.xml # '-' prefix means ok to fail
	@.max/basex/bin/basex src/resources/tests-result.xq
.PHONY: tests


check: ## vérification de la configuration
	@if [ -f 'config.xml' ]; then\
		.max/basex/bin/basex -q"validate:rng('config.xml', '.max/resources/max-configuration.rng')" || false;\
		echo 'MaX : Fichier config.xml valide.';\
	else\
  		echo 'MaX : Fichier de configuration manquant : config.xml';\
  		exit 1;\
  	fi
.PHONY: check

run:  check ## Lancement de MaX
	@.max/basex/bin/basexhttpstop || true
	@.max/basex/bin/basexhttp -h1234
.PHONY: run



clean: ## Supprime l'installation de MaX et les fichiers du projet
	@read -p "Êtes-vous sûr? [o/N] " ans && ans=$${ans:-N} ; \
    if [ $${ans} = o ] || [ $${ans} = O ]; then \
		rm -rf content_html;\
		rm -rf templates;\
		rm -rf autoroute;\
		rm -rf locales;\
		rm config.xml;\
		rm -rf .max;\
    else\
        echo "Opération annulée" ; \
    fi
.PHONY: clean


demo:  ## Installation d'une édition de démo
	@if [ ! -d '.max/basex' ]; then\
		echo 'Installation de MaX manquante, veuillez lancer : make install.';\
	else\
		cp -r .max/fixtures/max .max/basex/data/;\
		cp -r .max/fixtures/content_html .;\
		cp -r .max/fixtures/templates .;\
		cp -r .max/fixtures/autoroute .;\
		cp -r .max/fixtures/config.xml .;\
		cp -r .max/fixtures/locales .;\
		echo 'Demo installée avec succès.';\
	fi
.PHONY: demo


feed: ## Insertion de sources XML
	@if [ -z $(path) ]; then\
  		echo "USAGE : make feed path=/path/to/dir/";\
  		exit 1;\
	elif [ ! -d $(path) ]; then\
  		echo "Dossier "$(path)" introuvable !";\
  		echo "USAGE : make feed path=/path/to/dir/"; \
  		exit 1;\
  	fi;\
    $(shell .max/basex/bin/basex -c"OPEN max; ADD "$(path))\
    echo $(path)" ajouté à la base 'max'."
.PHONY: feed



help: ## Affiche cette aide
	@echo "\nChoisissez une commande. Les choix sont:\n"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[0;36m%-12s\033[m %s\n", $$1, $$2}'
	@echo ""
.PHONY: help
