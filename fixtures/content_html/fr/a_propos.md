# À propos

Page de contenu *{$lang}* du bundle `max-dumb-xml` pour [MaX-V2](https://git.unicaen.fr/pdn-certic/max-v2) rédigée au format markdown dans le fichier `content\_html/a_propos.md`.