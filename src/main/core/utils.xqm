xquery version "3.0";

(:~
: MaX "utils" module
:)
module namespace utils = "https://certic.unicaen.fr/max/utils";

import module namespace conf = 'https://certic.unicaen.fr/max/conf' at 'conf.xqm';
import module namespace database = 'https://certic.unicaen.fr/max/database' at 'database.xqm';
import module namespace errors = 'https://certic.unicaen.fr/max/errors' at 'errors.xqm';
import module namespace globals = 'https://certic.unicaen.fr/max/globals' at 'globals.xqm';


(:~ Returns the result of the 'doc-to-html' function of the active vocabulary bundle
 : @param $collection collection
 : @param $doc xml filename
 : @param $lang language
 : @return HTML page
 :)
declare function utils:doc-to-html($collection as xs:string?, $doc as xs:string, $lang as xs:string) as element(html){
    (:
     - execute doc-to-html func of the active vocabulary bundle
    :)
    try{
        let $ns := conf:get-vocabulary-namespace()
        let $xqFilePath := '../bundles/' || conf:get-vocabulary-bundle() || '/' || conf:get-vocabulary-xq-file()
        let $declaration := "declare variable $doc external; declare variable $lang external;"
        let $import := "import module namespace vocabulary = '"|| $ns ||"' at '" || $xqFilePath ||"';"
        let $documentPath := if ($collection) then $collection || '/' || $doc || '.xml' else $doc || '.xml'
        let $doc := database:get-document($collection || '/' || $doc || '.xml')
        let $query := 'vocabulary:doc-to-html($doc, $lang)'
        return xquery:eval($import ||  $declaration || $query, map {'doc' : $doc, 'lang': $lang})
    }
    catch repo:not-found {errors:error($err:code, $err:description, $err:line-number, $lang)}
};

(:declare function utils:eval-bundle-function($bundle as xs:string, $function as xs:string){
    try{
        let $ns := conf:get-bundle-namespace($bundle)
        let $xqFilePath := '../bundles/' || $bundle || '/' || conf:get-bundle-xq-file()
        let $declaration := "declare variable $doc external; declare variable $lang external;"
        let $import := "import module namespace vocabulary = '"|| $ns ||"' at '" || $xqFilePath ||"';"
        let $query := 'vocabulary:' || ||($doc, $lang)'
        return xquery:eval($import ||  $declaration || $query, map {'doc' : $doc, 'lang': $lang})
    }
    catch repo:not-found {errors:error($err:code, $err:description, $err:line-number, $lang)}
};:)


(:~ Returns bundles directory path
 : @return directory
 :)
declare function utils:get-bundles-path() as xs:string{
     db:option('webpath') || '/max/bundles/'
};


(:~ Returns an autoroute xq file path according to its name
 : @param $filename filename
 : @return file path
 :)
declare function utils:get-autoroute-file-path($filename as xs:string){
    let $userFile := file:current-dir() || $globals:autoroute-dir || '/' || $filename
    return if(file:exists($userFile))
           then $userFile
           else
            let $voc := conf:get-vocabulary-bundle()
            let $bundleFile:= utils:get-bundles-path()  || $voc || '/' || $globals:autoroute-dir || '/' || $filename
            return if(file:exists($bundleFile))
                   then $bundleFile
                    else fn:error(xs:QName('err:autoroute'),'no autoroute file found (' || $bundleFile || ')')
};



(:~
 : @return max locales path
 :)
declare function utils:get-locales-path() as xs:string{
    file:base-dir() || $globals:locales-dir
};

(:~
 : Returns consult route for an xml doc
 : @param $document document node
 : @param $lang language
 : @return string document route
 :)
declare function utils:get-document-route($document as document-node(), $lang as xs:string) as xs:string {
    '/' || $lang || fn:replace(fn:replace(base-uri($document),$database:max-db || '/',''),'.xml','.html')
};


(:~
 : Test if a bundle exists
 : @param $bundleName bundle name
 : @return boolean
 :)
declare function utils:bundle-exists($bundleName as xs:string) as xs:boolean{
    file:exists(utils:get-bundles-path() || $bundleName)
};

(:~
 : Return list of enabled bundles
 : @return bundle names
 :)
declare function utils:enabled-bundles() as xs:string*{
        for $b in file:list(utils:get-bundles-path())
            let $name := replace($b,'/','')
            return 
                if(not(file:exists(utils:get-bundles-path() || $b || '.ignore')))
                then $name else()
};











