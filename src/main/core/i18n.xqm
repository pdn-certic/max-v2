xquery version "3.0";
(:~
: "i18n" module
:)
module namespace i18n = 'https://certic.unicaen.fr/max/i18n';

import module namespace conf = 'https://certic.unicaen.fr/max/conf' at 'conf.xqm';
import module namespace utils = 'https://certic.unicaen.fr/max/utils' at 'utils.xqm';
(:~
 : Returns custom translation (from locales/)
 : @param $key translation key
 : @param $lang target language code
 : @return string
 :)
declare function i18n:translate ($key as xs:string, $lang as xs:string) as xs:string {
   try {
    let $translations := json:doc(i18n:getUserLocalesPath() || $lang || '.json')
    let $translation := string(($translations/json/*[local-name(.)=$key])[1]/text())
    return if($translation = '') then i18n:default-translate($key, $lang) else $translation
   }catch * {i18n:default-translate($key, $lang)}
};

(:~
 : Returns default translation (from vocabulary bundle locales)
 : @param $key translation key
 : @param $lang target language code
 : @return string
 :)
declare function i18n:default-translate ($key as xs:string, $lang as xs:string) as xs:string {
   try {
    let $translations := json:doc(i18n:getVocabularyLocalesPath()  || $lang || '.json')
    let $translation := string(($translations/json/*[local-name(.)=$key])[1]/text())
    return if($translation = '') then $key else $translation
   }catch * {$key}
};

(:~
 : Returns user locales path
 : @return string
 :)
declare function i18n:getUserLocalesPath() as xs:string{
    file:current-dir() || 'locales/'
};

(:~
 : Returns bundle locales path
 : @return string
 :)
declare function i18n:getVocabularyLocalesPath() as xs:string{
    let $voc := conf:get-vocabulary-bundle()
    let $repoPath := utils:get-bundles-path()
    return $repoPath || $voc || '/locales/'
};

(:~
 : Returns both user and vocabulary locales paths
 : @return sequence of strings
 :)
declare function i18n:getLocalePaths() as xs:string+{
    (i18n:getUserLocalesPath(), i18n:getVocabularyLocalesPath())
};

