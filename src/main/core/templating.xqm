xquery version "3.0";

(:~
: MaX "templating" module
:)
module namespace templating = "https://certic.unicaen.fr/max/templating";

import module namespace conf = 'https://certic.unicaen.fr/max/conf' at 'conf.xqm';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n' at 'i18n.xqm';
import module namespace utils= 'https://certic.unicaen.fr/max/utils' at 'utils.xqm';

(:~
 : Template rendering
 : @param $template template name
 : @param $map variables
 : @return HTML page
 :)
declare function templating:render($template as xs:string, $map as map(*)){
    let $filePath := templating:template-file($template)
    let $templateDoc := try {
        doc($filePath)
    }
    catch * {html:doc($filePath)}
    let $extends := $templateDoc//*:template/@*:data-extends
    let $render :=
        if($extends)
        then
            let $childRender := templating:eval-template($templateDoc, $map)
            let $parent := templating:eval-template(
            html:doc(templating:template-file( string($templateDoc//*:template/@data-extends))),
             $map)
            (: replace data-include:)
            let $withIncludes :=
                copy $c := $parent
                modify (
                  for $dataInclude in $c//*:template[@data-include]
                    let $templateName := string($dataInclude/@data-include)
                    let $templateDoc := doc(templating:template-file($templateName))
                    return replace node $dataInclude with templating:eval-template($templateDoc, $map)
                )
                return $c

             (: replace data-block :)
            let $withBlocks :=
                copy $c := $withIncludes
                modify (
                  for $dataBlock in $c//*:slot[@name]
                    let $blockName := string($dataBlock/@name)
                    return replace node $dataBlock with $childRender//*[@*:slot=$blockName]
                )
                return $c
            return $withBlocks
        else templating:eval-template($templateDoc, $map)


    return templating:post-render($render)



    (:return if(conf:is-dev-mode())
           then
            fold-left($bundles, $render, $f)
            (:let $devLink :=<a
                style="position: fixed; bottom :5px; left: 5px; background-color: #333; padding: 2px 5px; color: white" href="/about.html">?</a>
            return
                copy $c := $render
                modify (
                    insert node $devLink into $c//*:body
                )
                return $c:)
           else $render :)
};

(:~
 : Eval template as xquery
 : @param $templateDoc template document
 : @param $map variables
 : @return html node
 :)
declare function templating:eval-template($templateDoc as document-node(), $map as map(*)){
    let $params := map:put($map, 'active', fn:replace(fn:tokenize(request:path(),'/')[last()],'.html',''))
    let $declarations := string-join(
    for $var in map:keys($params)
                return "declare variable $" || $var || " external;")
    (:let $import := "import module namespace templating = 'https://certic.unicaen.fr/max/templating' at 'templating.xqm';":)
    let $packageDoc := doc(conf:get-package-file-path())
    let $imports := for $module in $packageDoc//*:xquery
                    let $namespace := $module/*:namespace/data()
                    let $file := $module/*:file/data()
                    let $name := tokenize($namespace,'/')[last()]
                    return "import module namespace " || $name ||" = '"|| $namespace || "' at '"|| $file || "';"
    return xquery:eval(string-join($imports) ||$declarations ||  serialize($templateDoc), $params)
};

(:~
 : Return template file path according to its name
 : @param $filename template filename
 : @return string
 :)
declare function templating:template-file($filename as xs:string){

    let $userFile := file:current-dir()|| 'templates/' || $filename
    return
        if(file:exists($userFile))
        then $userFile
        else
            let $voc := conf:get-vocabulary-bundle()
            let $bundleFile:= utils:get-bundles-path()  || $voc || '/templates/' || $filename
            return if(file:exists($bundleFile))
               then $bundleFile
               else fn:error(xs:QName('err:max-templating'),'no template found : ' || $userFile || ', ' || $bundleFile)

};

(:~
 : Return languages html list
 : @param $currentLang current lang
 : @return html ul node
 :)
declare function templating:languageSelect($currentLang as xs:string?) as node()?{
    let $languages := conf:get-languages()

    return
    if(count($languages) > 1)
        then
            let $routeSuffix := substring(request:path(), 4)
            return
                    <ul class="languages navbar-nav">
                    { for $l in $languages
                        return
                            <li class="nav-item"><a class="nav-link p-2 ms-1 me-1 {if($currentLang = $l) then 'active' else ''}"
                            href="/{$l || $routeSuffix}">{$l}
                            </a></li>
                    }
                    </ul>
        else ()
};


(:~
 : bundle static file url
 : @return string
 :)
declare function templating:static($bundleName as xs:string, $filename as xs:string) as xs:string{
    '/' || $bundleName || '/static/' || $filename
};


declare function templating:post-render($render as element()) as element(){
    let $bundles := utils:enabled-bundles()

    let $f := fn($result, $bundle) { 
        try {
            let $xqueryFile := utils:get-bundles-path() || $bundle || '/post-render.xq'
            return xquery:eval(xs:anyURI($xqueryFile), map {'html': $result})
        }
        catch err:FODC0002 {$result}
    }

    return fold-left($bundles, $render, $f)

};
