xquery version "3.0";

(:~
: MaX  errors module
:)
module namespace errors = 'https://certic.unicaen.fr/max/errors';
import module namespace templating = 'https://certic.unicaen.fr/max/templating'  at 'templating.xqm';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n'  at 'i18n.xqm';
import module namespace conf = 'https://certic.unicaen.fr/max/conf'  at 'conf.xqm';

(:~
 : Returns 404 error page
 : @return HTML page
 :)
declare
%output:method("html")
%output:html-version('5')
function errors:not-found($message, $lang as xs:string){
  let $tmpl := templating:template-file('error.html')
  let $detail := if(conf:is-dev-mode()) then $message else ''
  let $render := if($tmpl)
    then templating:render('error.html', map{'title': i18n:translate('not-found', $lang),'content' : $detail, 'lang' : $lang})
    else $message
  return(
  <rest:response>
    <http:response status="404">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log($message),
  $render)

};


(:~
 : Returns a 500 error page
 : @return HTML page
 :)
declare
%output:method("html")
%output:html-version('5')
function errors:error($code as xs:QName, $desc as xs:string, $line as xs:integer, $lang as xs:string) as element(html){
  let $tmpl := templating:template-file('error.html')
  let $detail := if(conf:is-dev-mode()) then string-join(($code,  $desc, 'line ' || $line),', ') else ''
  let $render := if($tmpl) then templating:render(
    'error.html',
    map{'title': i18n:translate('server-error', $lang),'content' : $detail, 'lang' : $lang})
    else errors:default-error-render($detail)
  return
  (<rest:response>
    <http:response status="500">if($tmpl)
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log($detail),
  $render)
};

(:~
 : Returns an errors page
 : @return HTML page
 :)
declare
%output:method("html")
%output:html-version('5')
function errors:errors($messages, $lang as xs:string?) as element(html){
  let $tmpl := templating:template-file('error.html')
  let $detail := if(conf:is-dev-mode())
    then  <ul>{
        for $m in $messages
        return <li>{$m}</li>
        }</ul>
    else ''
  let $render := if($tmpl)
    then templating:render(
    'error.html',
    map{'title': i18n:translate('server-error', $lang),'content' : $detail, 'lang' : $lang})
    else errors:default-error-render(fn:string-join($messages, ' / '))
  return(
  <rest:response>
    <http:response status="500">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log(fn:string-join($messages, ' / ')),
  $render)
};


declare %private function errors:default-error-render($message) as element(html){
    <html>
        <head>
            <title>MaX Error</title>
       </head>
       <body>
        <p>{message}</p>
       </body>
    </html>
};