xquery version "3.0";

(:~
: MaX RestXQ routes
:)
module namespace routes = 'https://certic.unicaen.fr/max/routes';

import module namespace conf = 'https://certic.unicaen.fr/max/conf' at 'conf.xqm';
import module namespace database = 'https://certic.unicaen.fr/max/database' at 'database.xqm';
import module namespace templating = 'https://certic.unicaen.fr/max/templating' at 'templating.xqm';
import module namespace errors = 'https://certic.unicaen.fr/max/errors' at 'errors.xqm';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n' at 'i18n.xqm';
import module namespace utils = 'https://certic.unicaen.fr/max/utils' at 'utils.xqm';
import module namespace globals = 'https://certic.unicaen.fr/max/globals' at 'globals.xqm';
import module namespace markdown = 'https://certic.unicaen.fr/max/markdown' at 'markdown.xqm';

(:~
 : Returns the main index page for the default language
 : @return HTML page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:encoding("UTF-8")
%rest:path("/{$path=(index.html)?")
function routes:index($path as xs:string?){
   routes:page($conf:default-lang, 'index')
};

(:~ Returns the main index page for a specific language
 : @return HTML page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:encoding("UTF-8")
%rest:path("/{$lang=[a-z]{2}}")
function routes:lang-index($lang as xs:string) as element(html){
   routes:page($lang, 'index')
};

(:~ Returns an XML document transformed as HTML
 : @param $lang language
 : @param $collection collection
 : @param $doc xml filename
 : @return HTML page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/{$lang=[a-z]{2}}/{$collection=.+}/{$doc=[a-zA-Z0-9_\-]+}.html")
function routes:document($lang as xs:string, $collection as xs:string, $doc as xs:string) as element(html){
    try{
        utils:doc-to-html($collection, $doc, $lang)
    }
    catch err:max-database{errors:error($err:code, $err:description, $err:line-number, $lang)}
};

(:~
 : Returns a page (HTML or Markdown) from the "content_html" directory
 : @param $lang language
 : @param $page  file or unknown path
 : @return HTML page
 : @examples <pre>routes:pages('fr', 'my-page.html') <br/>
 : routes:pages($conf:default-lang, 'my-page.md')</pre>
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/{$lang=[a-z]{2}}/pages/{$path=.+}.html")
function routes:page($lang as xs:string, $path as xs:string){
        try {
            let $mdFile := file:current-dir() || $globals:content-html-dir || '/' || $lang || '/' || $path || '.md'
            let $htmlFile := file:current-dir() || $globals:content-html-dir || '/' || $lang || '/' || $path || '.html'
            let $isMd := file:exists($mdFile)
            let $content:= if($isMd) then file:read-text($mdFile) else file:read-text($htmlFile)
            let $res := replace($content, '\{\$lang\}', $lang)
            let $content := if ($isMd) then markdown:parse($res) else html:parse($res)
            return templating:render('page.html', map{'content' : $content, 'lang': $lang})
        }
        catch file:not-found {errors:not-found($err:code || ' - ' ||$err:description, $lang)}
        catch err:FODC0002 {errors:not-found($err:code || ' - ' ||$err:description, $lang)}
        catch err:max-templating {errors:error($err:code, $err:description, $err:line-number, $lang)}
        catch err:max-conf {errors:error($err:code, $err:description, $err:line-number, $lang)}
};


(:~ Returns the result of autoroute/$page.xq if exists, else transforms $page document as HTML
 : @param $lang language
 : @param $page xq filepath or document path
 : @return HTML page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/{$lang=[a-z]{2}}/{$page=[a-zA-Z0-9_\-]+}.html")
function routes:autoroute($lang as xs:string, $page as xs:string){
    try {
        let $xqueryFile := utils:get-autoroute-file-path($page || '.xq')
        return xquery:eval(xs:anyURI($xqueryFile), map {'lang': $lang})
    }
    catch * {
            try {
                let $content := utils:doc-to-html((), $page, $lang)
                return $content
            }
            catch err:max-database {
                errors:error($err:code, $err:description, $err:line-number, $lang)
            }
            catch err:XPTY0004 {
                errors:not-found($page || ".xq autoroute file not found"  ,$lang)
            }
            catch * {errors:error($err:code, $err:description, $err:line-number, $lang)}
    }
};

(:~
 : Returns a static file.
 : @param  $filepath  file or unknown path
 : @return rest binary data
 :)
declare %rest:path("/{$bundle}/static/{$filepath=.+}")
function routes:file(
  $bundle as xs:string, $filepath as xs:string
) as item()+ {
    try {
    let $path := utils:get-bundles-path() || $bundle || '/static/' || $filepath
    return (
      web:response-header(
        { 'media-type': web:content-type($path) },
        { 'Cache-Control': 'max-age=3600,public', 'Content-Length': file:size($path) }
      ),
      file:read-binary($path)
    )
    }
    catch file:not-found {web:error(404, "The requested resource cannot be found.")}

};



