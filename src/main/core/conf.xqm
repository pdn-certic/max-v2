xquery version "3.0";

(:~
: MaX "conf" module
: Reads configuration from the MaX config.xml file
:)
module namespace conf = 'https://certic.unicaen.fr/max/conf';

import module namespace utils = 'https://certic.unicaen.fr/max/utils' at 'utils.xqm';

declare namespace max="http://certic.unicaen.fr/max/ns/1.0";
declare namespace expath="http://expath.org/ns/pkg";

(: =============== GLOBAL VARS =============== :)

(:~ config file name:)
declare variable $conf:config-file := 'config.xml';

(:~ package file name:)
declare variable $conf:package-file := 'expath-pkg.xml';

(:~ default lang:)
declare variable $conf:default-lang := 'fr';

(:~ dev mode name:)
declare variable $conf:dev-mode := 'dev';
(: =========================================== :)


(:~
 : Returns MaX configuration
 : @return element
 :)
declare function conf:get-configuration() as element(){
	    doc(conf:get-configuration-path())/max:configuration
};

(:~
 : Returns the MaX configuration file path
 : @return file path
 :)
declare function conf:get-configuration-path() as xs:string{
	file:current-dir() || $conf:config-file
};

(:~
 : Returns then MaX expath package file path
 : @return file path
 :)
declare function conf:get-package-file-path() as xs:string{
    conf:get-resources-path() || $conf:package-file
};

(:~
 : Returns MaX resources dir path
 : @return dir path
 :)
declare function conf:get-resources-path() as xs:string{
    file:current-dir() ||'.max/resources/'
};

(:~
 : Returns the active vocabulary name
 : @return vocabulary name
 :)
declare function conf:get-vocabulary-bundle() as xs:string{
	let $bundle := string(conf:get-configuration()/@vocabulary-bundle)
	return
	if(not($bundle))
	    then fn:error(xs:QName('err:max-conf'), 'No vocabulary bundle was found in your config.xml file')
	else
	    if(not(utils:bundle-exists($bundle)))
	    then fn:error(xs:QName('err:max-conf'), 'Bundle not found : ' || $bundle ||  ' in ' || utils:get-bundles-path())
	    else $bundle
};

(:~
 : Returns the list of available languages
 : @return sequence
 :)
declare function conf:get-languages() as xs:string*{
	conf:get-configuration()//max:languages/max:language/text()
};


(:~
 : Returns the corpus title
 : @return corpus title
 :)
declare function conf:get-title() as xs:string{
	string(conf:get-configuration()/max:title)
};

(:~
 : Returns true() when dev mode is enabled
 : @return dev mode or not
 :)
declare function conf:is-dev-mode() as xs:boolean{
	string(conf:get-configuration()/@env) = $conf:dev-mode
};



(:~
 : Returns the namespace of the active vocabulary bundle
 : @return bundle namespace
 :)
declare function conf:get-vocabulary-namespace() as xs:string{
    conf:get-bundle-namespace(conf:get-vocabulary-bundle())
};

(:~
 : Returns the namespace of a bundle
 : @return bundle namespace
 :)
declare function conf:get-bundle-namespace($bundleName as xs:string) as xs:string{
    let $pkgFile := utils:get-bundles-path() || $bundleName || '/' || $conf:package-file
    return doc($pkgFile)//expath:xquery/expath:namespace/text()
};

(:~
 : Returns the active vocabulary bundle xq file path
 : @return file path
 :)
declare function conf:get-vocabulary-xq-file() as xs:string{
    conf:get-bundle-xq-file(conf:get-vocabulary-bundle())

};

(:~
 : Returns bundle xq file path
 : @return file path
 :)
declare function conf:get-bundle-xq-file($bundleName as xs:string) as xs:string{
    let $pkgFile := utils:get-bundles-path() || $bundleName || '/' || $conf:package-file
    return string(doc($pkgFile)/expath:package/@abbrev) || '/' || doc($pkgFile)//expath:xquery/expath:file/text()
};

(:~
 : Returns MaX version number
 : @return version
 :)
declare function conf:get-max-version() as xs:string{
    let $version := string(doc(conf:get-package-file-path())/expath:package/@version)
    return
        if(conf:is-dev-mode())
        then
            try {
            let $commit:= proc:system('git', ('rev-parse', 'HEAD'))
            return $version || ' - commit ' ||$commit
            }
            catch * {
               $version
            }
        else $version
};




