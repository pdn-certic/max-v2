xquery version "3.0";

(:~
: MaX "database" module
:)
module namespace database = 'https://certic.unicaen.fr/max/database';

(: =============== GLOBAL VARS =============== :)
(:~ MaX db name:)
declare variable $database:max-db := "max";


(:~
 : Returns an XML document of MaX DB
 : @param $documentPath document path
 : @return document
 :)
declare function database:get-document($documentPath as xs:string) as document-node()?{
    try {
	 db:get($database:max-db, $documentPath)
	}
	catch db:open {
	    fn:error(xs:QName('err:max-database'), 'max database does not exist !')
	}

};


(:~
 : Returns an identified fragment in a document of MaX db
 : @param $documentPath document path
 : @param  $id  fragment id
 : @return identified fragment in a document of max db
 :)
declare function database:get-element-by-id($documentPath, $id as xs:string) as element()?{
	database:get-document($documentPath)//*[@*:id=$id]
};


