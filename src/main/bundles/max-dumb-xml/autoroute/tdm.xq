xquery version "3.0";

import module namespace database = 'https://certic.unicaen.fr/max/database' at '../../../core/database.xqm';
import module namespace templating = 'https://certic.unicaen.fr/max/templating' at '../../../core/templating.xqm';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n' at '../../../core/i18n.xqm';
import module namespace utils = 'https://certic.unicaen.fr/max/utils' at '../../../core/utils.xqm';

declare variable $lang  external;

let $tdm :=
    <div>
        <h4>{i18n:translate('tdm',$lang)}</h4>
        <ul>{
        for $doc in collection($database:max-db)
            return <li><a href="{utils:get-document-route($doc, $lang)}">{replace(base-uri($doc),'/' || $database:max-db || '/','')}</a></li>
            }
        </ul>
        <p>
            Générée par <code>max-dumb-xml/autoroute/tdm.xq</code>
        </p>
    </div>

return
    templating:render('page.html', map{'content': $tdm, 'lang': $lang})
