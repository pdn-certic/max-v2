xquery version "3.0";

(:~
: MaX "dumb-xml" bundle
:)
module namespace dumb-xml = "https://certic.unicaen.fr/max/bundles/dumb-xml";

import module namespace templating = "https://certic.unicaen.fr/max/templating" at '../../../core/templating.xqm';
import module namespace i18n = "https://certic.unicaen.fr/max/i18n" at '../../../core/i18n.xqm';
import module namespace database = "https://certic.unicaen.fr/max/database" at '../../../core/database.xqm';
import module namespace errors = "https://certic.unicaen.fr/max/errors" at '../../../core/errors.xqm';

declare function dumb-xml:doc-to-html($document as document-node(), $lang as xs:string){
    (:generate random http link to an identified node in $document:)
    let $identifiers:= $document//*/@xml:id/string()
    let $n := count($identifiers)
    let $r := random:integer($n) + 1
    let $target := $identifiers[$r]
    let $href := fn:replace(request:path(),'.html', '') || '/id/' || $target || '.html'

    let $content := <div>
                        <h2>{i18n:translate('raw', $lang)} | {fn:replace(fn:base-uri($document), '/' || $database:max-db || '/', '')}</h2>
                        {$document}
                        <nav>
                            <h3><a href="{$href}">{i18n:translate('random-link', $lang)} ({$target})</a></h3>
                        </nav>
                    </div>
    return templating:render('page.html', map {'content': $content, 'lang': $lang})
};


(:~
 : @return an identified node as dumb html
 :)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:encoding("UTF-8")
%rest:path("/{$lang=[a-z]{2}}/{$docPath=.+}/id/{$id}.html")
function dumb-xml:id-to-html($lang as xs:string, $docPath as xs:string, $id as xs:string){
   try {
    let $node := database:get-element-by-id($docPath || '.xml', $id)
    let $content := <div>
                        <h1>id: {$id}</h1>
                        {
                            if($node) then $node else 'Node with id "' || $id || '" was not found'
                        }
                    </div>
    return templating:render('page.html', map {'content': $content, 'lang': $lang})
   }
   catch * {
        errors:error($err:code, $err:description, $err:line-number, $lang)
   }
};
