<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xqdoc="http://www.xqdoc.org/1.0"
                exclude-result-prefixes="#all"
>

  <xsl:output method="xml" encoding="UTF-8"/>


  <xsl:template match="/">
    <html>
      <head>
        <title>MaX XQuery Documentation</title>
        <link rel='stylesheet' type='text/css' href='/static/max.css'/>
      </head>
      <body class="h-100">
        <div class="wrapper">
          <!-- modules nav menu -->
          <aside>
            <nav>
              <h2>Modules</h2>
              <ul>
                <xsl:for-each select="//xqdoc:module">
                  <li>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:value-of select="concat('#module-',xqdoc:name)"/>
                      </xsl:attribute>
                      <xsl:value-of select="xqdoc:name"/>
                    </a>
                  </li>
                </xsl:for-each>
              </ul>
            </nav>
          </aside>
          <main>
            <h1>MaX XQuery Documentation</h1>
            <xsl:apply-templates select="//xqdoc:xqdoc"/>
          </main>
        </div>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="xqdoc:xqdoc">
    <h2>
      <xsl:attribute name="id">
        <xsl:value-of select="concat('module-',./xqdoc:module/xqdoc:name)"/>
      </xsl:attribute>
      # <xsl:value-of select="./xqdoc:module/xqdoc:name"/>
    </h2>
    <div class="module-desc">
      <xsl:value-of select="xqdoc:module/xqdoc:comment/xqdoc:description"/>
    </div>
    <xsl:apply-templates select=".//xqdoc:functions/xqdoc:function"/>
  </xsl:template>



  <xsl:template match="xqdoc:function">
    <div class="function">
      <h3>
        <xsl:value-of select="xqdoc:name"/>
      </h3>

      <xsl:if test="xqdoc:comment/xqdoc:description">
        <div class="description">
          <h4>Description</h4>
          <p><xsl:value-of select="xqdoc:comment/xqdoc:description"/></p>
        </div>
      </xsl:if>
      <div class="signature">
        <h4>Signature</h4>
          <div>
            <pre>
              <xsl:value-of select="concat(xqdoc:name, ' ', tokenize(xqdoc:signature,xqdoc:name)[last()])"/>
            </pre>
          </div>
      </div>


      <xsl:if test="xqdoc:parameters/xqdoc:parameter">
        <div class="parameters">
          <h4>Parameters</h4>
          <ul>
            <xsl:for-each select="xqdoc:parameters/xqdoc:parameter">
              <li class="parameter">
                <strong><xsl:value-of select="xqdoc:name"/>: </strong>
                <xsl:value-of select="xqdoc:type"/>
                <xsl:if test="xqdoc:description">
                  - <span><xsl:value-of select="xqdoc:description"/></span>
                </xsl:if>
              </li>
            </xsl:for-each>
          </ul>
        </div>
      </xsl:if>

      <xsl:if test="xqdoc:return">
        <div class="return">
          <h4>Return Type</h4>
          <p><xsl:value-of select="xqdoc:return/xqdoc:type"/></p>
        </div>
      </xsl:if>


      <xsl:if test="xqdoc:comment/xqdoc:custom[@tag='examples']">
        <div class="examples">
          <h4>Examples</h4>
          <p><xsl:copy-of select="xqdoc:comment/xqdoc:custom"/></p>
        </div>
      </xsl:if>
    </div>
  </xsl:template>

</xsl:stylesheet>
