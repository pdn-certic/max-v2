Bundle max-dev
====

Fonctionnalités actives uniquement en environnement "dev".

```xml
<configuation env="dev"...>...</configuration>
```

## Routes
- `/bundles/max-dev/index.html` : affiche les informations de configuration et d'environnement
- `/bundles/max-dev/doc.html` : documentation API XQuery de MaX