xquery version "3.0";

import module namespace conf = 'https://certic.unicaen.fr/max/conf' at '../../core/conf.xqm';

declare variable $html  external;
    
if(conf:is-dev-mode())
then
let $devLink :=<a
    style="position: fixed; bottom :5px; left: 5px; background-color: #333; padding: 2px 5px; color: white" href="/bundles/max-dev/index.html">?</a>
return
    copy $c := $html
    modify (
        insert node $devLink into $c//*:body
    )
    return $c
else $html