xquery version "3.0";

(:~
: MaX dev
:)

module namespace max-dev = 'https://certic.unicaen.fr/max/max-dev';
import module namespace database = 'https://certic.unicaen.fr/max/database' at '../../core/database.xqm';
import module namespace conf = 'https://certic.unicaen.fr/max/conf' at '../../core/conf.xqm';
import module namespace errors = 'https://certic.unicaen.fr/max/errors' at '../../core/errors.xqm';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n' at '../../core/i18n.xqm';
import module namespace utils = 'https://certic.unicaen.fr/max/utils' at '../../core/utils.xqm';


(:~ Returns MaX technicals information - only in a dev environment
 : @return HTML page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/bundles/max-dev/index.html")
function max-dev:index() as element(){
    if(conf:is-dev-mode())
    then
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta charset="UTF-8"/>
            <title>MaX - infos</title>
            <link rel='stylesheet' type='text/css' href='static/max.css'/>
        </head>
        <body>

            <h1> <a href="/{$conf:default-lang}/pages/index.html">MaX</a></h1>
            <h2>Version <code>{conf:get-max-version()}</code></h2>
            <h3>BaseX {db:system()//*:version}</h3>
            <section>
                <h2>Base de données XML <strong>{$database:max-db} </strong></h2>
                    {if(db:exists($database:max-db))
                        then
                            <div>
                                <ul>{
                                for $d in db:list($database:max-db)
                                return <li>{$d}</li>
                                }</ul>
                            </div>
                        else <span style='color:red;'> NOK</span>
                    }
            </section>
            {max-dev:configuration-as-html()}
            <section>
                <h2>Contexte</h2>
                <ul>
                    <li><strong>file:current-dir </strong> {file:current-dir()}</li>
                    <li><strong>file:base-dir </strong> {file:base-dir()}</li>
                </ul>
            </section>
            <section>
                <h2>Locales</h2>
                <ul>
                 {for $p in i18n:getLocalePaths()
                    return <li>{$p}</li>
                 }
                 </ul>
            </section>
            <section>
                <h2>Bundles</h2>
                <ul>
                    {
                        for $b in file:list(utils:get-bundles-path())
                            let $name := replace($b,'/','')
                            let $enabled := not(file:exists(utils:get-bundles-path() || $b || '.ignore'))

                            return if($enabled) 
                                    then 
                                        <li>{$name} <input 
                                                    type="checkbox" 
                                                    checked="checked" 
                                                    title="désactiver le bundle" 
                                                    onclick="fetch('/bundles/max-dev/setbundle?enabled=' + this.checked + '&amp;bundle={$name}').then(() => window.location.reload(true));"/></li>
                                    else <li>{$name} <input 
                                                    type="checkbox" 
                                                    title="activer le bundle" 
                                                    onclick="fetch('/bundles/max-dev/setbundle?enabled=' + this.checked + '&amp;bundle={$name}').then(() => window.location.reload(true));"/></li>                                
                    }
                </ul>
            </section>
            <section>
                <h2>Routes</h2>
                {max-dev:route-list()}
            </section>

            <section>
                <h2>Documentation</h2>
                <div>
                    <a href="/bundles/max-dev/doc.html">Consulter l'API XQuery de MaX</a>
                </div>
            </section>
        </body>
    </html>
    else errors:not-found('', $conf:default-lang)
};


(:~
 : MaX functions documentation.
 : Returns an HTML documentation for MaX Xqueries functions.
 : @return HTML page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/bundles/max-dev/doc.html")
function max-dev:functions-documentation(){
    if(conf:is-dev-mode())
    then
        let $toIgnore := ('globals.xqm','markdown.xqm')
        let $packageDoc := doc(conf:get-package-file-path())
        let $xqDocs := <div>{for $module in $packageDoc//*:xquery/*:file where not(string($module) = $toIgnore)
                        return inspect:xqdoc(db:option('webpath') || '/max/core/' || string($module))
                    }
                    </div>

        return xslt:transform($xqDocs, doc(utils:get-bundles-path()  || 'max-dev/doc-2-html.xsl'))
    else errors:not-found('', $conf:default-lang)
};


declare %private function max-dev:configuration-as-html() as element(){
    let $config := conf:get-configuration()
    return
        <section>
            <h2>Configuration (config.xml)</h2>
            <ul>
                <li><strong>vocabulaire : </strong>{conf:get-vocabulary-bundle()} ({conf:get-vocabulary-namespace()})</li>
                <li><strong>langues : </strong>{fn:string-join(conf:get-languages(),', ')} </li>
            </ul>
        </section>
};

(:~
 : @return routes list as html
 :)
declare %private function max-dev:route-list() as element(){
    let $wadl:=rest:wadl()
    let $maxRoutes := <ul>{
        for $r in $wadl//*:resource[@*:path]
        return
            let $path:= string($r/@*:path)
            let $method := string($r/*:method/@name)
            (:ignore other basex webapp routes:)
            return if(fn:starts-with($path,'/dba') or $path='' or fn:starts-with($path,'/chat') or not(contains($method,'GET')))
                   then ()
                   else 
                     if(not(contains($path, '$')))
                            then <li><a target="_blank" href="{$path}">{$path}</a></li>
                            else <li>{$path}</li>
     }</ul>
    return $maxRoutes

};




declare %rest:path("/bundles/max-dev/setbundle")
%rest:query-param("bundle","{$bundle}")
%rest:query-param("enabled","{$enabled}")
function max-dev:set-bundle-enabled($bundle as xs:string, $enabled as xs:boolean) as empty-sequence(){
    let $ignorePath := utils:get-bundles-path() || $bundle || '/.ignore'
    return 
        if($enabled)
            then file:delete($ignorePath)
        else file:write($ignorePath, '')  
};