module namespace max-export = 'https://certic.unicaen.fr/max/max-export';

import module namespace database = 'https://certic.unicaen.fr/max/database' at '../../core/database.xqm';

(:~ Exports MaX XML sources as zip
 : @return Zip file
 :)
declare
%rest:GET
%output:media-type("application/octet-stream")
%rest:query-param("indent", "{$indent}", 'false')
%rest:path("/sources.zip")
function max-export:sources($indent as xs:string) as item()+{
    try{
        let $zipName := 'sources.zip'
        let $tmpDir := file:create-temp-dir('max-export', $database:max-db)
        let $dest := $tmpDir ||'/'||$zipName
        return (
               db:export($database:max-db, $tmpDir, map { 'indent': if($indent='true') then true() else false()}),
               max-export:make-zip($tmpDir, $dest),
                <rest:response>
                    <http:response status="200">
                        <http:header name="content-type" value="application/zip"/>
                        <http:header name="Content-Disposition" value="attachment; filename={$zipName}"/>
                    </http:response>
                </rest:response>,
                file:read-binary($dest)
                )
    }
    catch * {$err:description}
};



(:~
 : Returns zip file
 : @param $directory dir to zip
 : @param $dest destination dir
 : @return binary zipped file
 :)
declare %private function max-export:make-zip($directory as xs:string, $dest as xs:string) as empty-sequence(){
    let $files := file:list($directory, true(), '*.xml')
    let $zip   := archive:create($files,
            for $file in $files
            return file:read-binary($directory || $file)
    )
    return file:write-binary($dest, $zip)
};
