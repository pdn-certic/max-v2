xquery version "3.0";

let $output := "tests-output.xml"
let $nl := "&#10;"
let $doc := doc("../../" || $output)

let $res :=
    for $ts in $doc//testsuite
     return "* " || $ts/@name || " : tests : " || $ts/@tests  || " / failures : " || $ts/@failures  || " / errors : " || $ts/@errors || $nl

return $nl || $res || $nl || '=> Tests done in ' || $doc/testsuites/@time || $ nl
        || $nl || "Full report available in " || $output || $nl
