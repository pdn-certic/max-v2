module namespace test = 'https://certic.unicaen.fr/max/tests';

import module namespace conf = 'https://certic.unicaen.fr/max/conf' at '../main/core/conf.xqm';


declare %unit:test function test:get-vocabulary-bundle(){
    unit:assert-equals(conf:get-vocabulary-bundle(), "max-dumb-tei-bundle")
 };

 declare %unit:test function test:get-languages() {
    let $languages-ok := ('fr','en')
    let $languages-nok := ('fr','es')
    return (
            unit:assert-equals(conf:get-languages(), $languages-ok),
            unit:assert(conf:get-languages()=$languages-nok, fn:false())
           )
};

declare %unit:test function test:get-title(){
	 unit:assert(conf:get-title() != "", fn:true)
};


declare %unit:test function test:is-dev-mode(){
	unit:assert(conf:is-dev-mode(), fn:true)
};
